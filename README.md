# Please help by citing this article and methods as follows:  
### Mukherjee S. Quiescent stem cell marker genes in glioma gene networks are sufficient to distinguish between normal and glioblastoma (GBM) samples. Sci Rep. 2020 Jul 2;10(1):10937. doi: 10.1038/s41598-020-67753-5. PMID: 32616845; PMCID: PMC7363816.  


The database file in .db format is provided as a ziped file 'GBMDrugGeneCisDisease', please unzip to use. 
The jupyter notebook, "sqlite3_pandas_Jupyter_queries.html" can be previewed using this link,
https://htmlpreview.github.io/?https://github.com/smukher2/Glioblastome_Drug_Gene_Disease_Database_SQL/blob/master/sqlite3_pandas_Jupyter_queries.html

Objective: A database to store and predict patient glioblastoma grade, cell type enrichment in tumor and drugs for therapeutic intervention using changes in patient gene expression and genetic variation.  

a)	The primary objective of the design was to be able to determine the patient tumor grade using gene expression data. As shown with the example of ‘Patient_Grade4_test_data’ the pipeline is able to predict the Glioblastoma grade of the patient sample, which here is Grade 4. This is shown in query and report ‘PatientTestCancerGrade_query2’
b)	The other objective of the dataset was to get information about the possible drugs that can be used for the patient based on gene expression profile. As shown with the example of ‘Patient_Grade4_test_data’ the pipeline is able to predict potential Drugs and shows known Disease associated genes/variations in the patient genes. This is shown in query and report ‘PatientTestDrugDisease_query4’. 
c)	Additionally, potential regulators of these genes i.e. cis-regulators are predicted from this pipeline. As shown with example of ‘Patient_Grade4_test_data’ the pipeline is able to predict cis regulators for the patient genes. This is shown in query and report ‘PatientTestDrugDisease_query3’. 
 
This database will increase the efficiency of users who use it for research in glioblastoma and also to classify patient tumors clinically. Instead of visiting multiple databases in one place the user can run any new patient’s tumor gene expression profile and get an idea of what drugs/treatment ragime to potentially suggest. 
