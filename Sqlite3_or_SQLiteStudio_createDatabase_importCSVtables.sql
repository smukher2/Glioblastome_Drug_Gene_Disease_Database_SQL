/*Create empty .db database in DB Manager or use SQLiteStudio
Import csv UTF-8 file to table using SQLite Studio using settings encoding=UTF-8, first row=field names, field separator=,(comma).
Alternatively, use sqlite3.exe to import csv file (non UTF-8 encoding)  
The .open, .mode, .import and .schema commands are for sqlite3.exe. For SQLiteStudio tabs provide these functionalities.
For ease of use of sqlite3.exe, use this file i.e. run the sqlite3.exe form the same folder as the csv files and .db file*/

/*SQLite version 3.21.0 2017-10-24*/

/*This .open command opens a pre-existing .db database file or creates a .db database if it does not exist*/
.open GBMDrugGeneCisDisease.db
.mode csv

/*TABLE1=CancerGradeUniqTable 
/* Import csv file to a database and create table with it in one single command*/
/* not necessary to provide path of the csv file if in the same folder as the sqlite3.exe*/
/*.import c:/sqlite/CancerGradeUniqTable.csv CancerGradeUniqTable*/
.import CancerGradeUniqTable.csv CancerGradeUniqTable
.schema CancerGradeUniqTable
/*Result
CREATE TABLE CancerGradeUniqTable(
  "gene" TEXT,
  "Given_Grade" TEXT,
  "Given_Grade_avgFPKM" TEXT,
  "otherGrades_avgFPKM" TEXT,
  "FC_GivenGrade_by_others_Grades" TEXT
);
*/

/* Disadvantage of this above method is that the default import field type is always TEXT, but the data contains other types too. 
So lets drop table and import to a pre-existing table with proper field types*/
DROP TABLE IF EXISTS CancerGradeUniqTable;

CREATE TABLE IF NOT EXISTS CancerGradeUniqTable (
	gene TEXT PRIMARY KEY,
	Given_Grade INTEGER,
	Given_Grade_avgFPKM NUMERIC,
	otherGrades_avgFPKM NUMERIC,
	FC_GivenGrade_by_others_Grades NUMERIC
);

/* import database and select mode csv if have not yet done that
.open GBMDrugGeneCisDisease.db
.mode csv*/
/*Now lets import our csv file into this*/
.import CancerGradeUniqTable.csv CancerGradeUniqTable
.schema CancerGradeUniqTable
/*Result
CREATE TABLE CancerGradeUniqTable (
        gene TEXT PRIMARY KEY,
        Given_Grade INTEGER,
        Given_Grade_avgFPKM NUMERIC,
        otherGrades_avgFPKM NUMERIC,
        FC_GivenGrade_by_others_Grades NUMERIC
);
*/

/*TABLE2=GeneInformationEdTable
/* create table note, we want to introduce the foreign key here*/
CREATE TABLE IF NOT EXISTS GeneInformationEdTable (
	gene TEXT PRIMARY KEY,
    refseq TEXT,
	ucsc_id TEXT,
	refseq_chr TEXT,
	refseq_start INTEGER,
	refseq_end INTEGER,
	strand TEXT,
	ensemble_id TEXT,
	ensemble_chr TEXT,
	ensemble_gene_start INTEGER,
	ensemble_gene_end INTEGER,
	Gene_description_ensemble TEXT,
	gene_id TEXT,
	gene_name_panther TEXT,
	Panther_family TEXT,
	Panther_protein_class TEXT,
    FOREIGN KEY (gene) REFERENCES CancerGradeUniqTable(gene)
);
/*Now lets import the csv file as before*/
.import GeneInformationEdTable.csv GeneInformationEdTable
.schema GeneInformationEdTable

/*TABLE3=CisRegulators5kbTable*/
CREATE TABLE CisRegulators5kbTable (
	CRR_hg19_id	TEXT  PRIMARY KEY,
	Chr_CRR	TEXT,
	Start_CRR INTEGER,
	End_CRR INTEGER,
	Strand_CRR TEXT,
	Annotation_of_CRR TEXT,
	Detailed_Annotation_of_CRR TEXT,
	Distance_to_TSS INTEGER,
	gene TEXT,
	gene_nearest_PromoterID TEXT,
	gene_Entrez_ID TEXT,
	gene_nearest_Unigene TEXT,
	gene_nearest_Refseq TEXT,
	gene_nearest_Ensembl TEXT,
	gene_nearest_Alias TEXT,
	gene_nearest_Description TEXT,
	gene_nearest_Type TEXT,
	NCBI_link TEXT,
	uniprot_link TEXT,
	UCSC_genome_browser	TEXT,
	FOREIGN KEY (gene) REFERENCES CancerGradeUniqTable(gene)
);

/*import csv file as before*/
.import CisRegulators5kbTable.csv CisRegulators5kbTable
.schema CisRegulators5kbTable
/*did not use got foreign key syntax error*
FOREIGN KEY (gene) REFERENCES CancerGradeUniqTable(gene),
	FOREIGN KEY (gene) REFERENCES GeneInformationEdTable(gene)
DROP TABLE IF EXISTS GeneInformationEdTable;
*/

/*TABLE4=DiseaseAssociationEdTable*/
CREATE TABLE DiseaseAssociationEdTable (
	id_gene_disease_association TEXT PRIMARY KEY,
	gene TEXT,
	doid_code TEXT,
	doid_name TEXT,
	pubmeds_max INTEGER,
	score_max NUMERIC,
	score_mean NUMERIC,
	associationType TEXT,
	source TEXT,
	FOREIGN KEY (gene) REFERENCES CancerGradeUniqTable(gene)
);

/*lets import the csv as before */
.import DiseaseAssociationEdTable.csv DiseaseAssociationEdTable
.schema DiseaseAssociationEdTable

/*TABLE5=DrugInteractionEdTable*/
CREATE TABLE DrugInteractionEdTable (
	id_gene_drug_disease TEXT PRIMARY KEY,
	gene TEXT,
	entrez_id INTEGER,
	interaction_types TEXT,
	drug_name TEXT,
	drug_claim_name TEXT,
	drug_claim_primary_name TEXT,
	MOLECULE_CHEMBL_ID TEXT,
	MOLECULE_NAME TEXT,
	MOLECULE_TYPE TEXT,
	interaction_claim_source TEXT,
	MESH_ID TEXT,
	MESH_HEADING TEXT,
	EFO_ID TEXT,
	EFO_NAME TEXT,
	MAX_PHASE INTEGER,
	FIRST_APPROVAL TEXT,
	USAN_YEAR TEXT,
	REFS TEXT,
	FOREIGN KEY (gene) REFERENCES CancerGradeUniqTable(gene)
);

/*lets import the csv as before */
.import DrugInteractionEdTable.csv DrugInteractionEdTable
.schema DrugInteractionEdTable

/*Example of a test patient gene expression table to be queried against the database
TABLE6=Patient_Grade4_test_data*/
CREATE TABLE Patient_Grade4_test_data (
	id_row_number TEXT PRIMARY KEY,
	gene TEXT,
	CGGA_D30 NUMERIC,
	FOREIGN KEY (gene) REFERENCES CancerGradeUniqTable(gene)
);

/*lets import the csv as before */
.import Patient_Grade4_test_data.csv Patient_Grade4_test_data
.schema Patient_Grade4_test_data


































