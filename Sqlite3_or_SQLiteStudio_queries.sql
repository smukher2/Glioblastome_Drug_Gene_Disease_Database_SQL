/*For ease of use of sqlite3.exe, use this file i.e. run the sqlite3.exe form the same folder as the csv files and 
.db file These queries can be run on sqlite3.exe or SQLite version 3.21.0 2017-10-24
Jupyter notebook version of the queries is also provided as a third alternative*/


/* QUERY1
In this first step a new patient gene expression data (FPKM normalized) with gene_symbol will be compared against 
pre-existing gene expression data from Glioblastoma patients in the database in CancerGradeUniqTable.
The >1.5 fold change cutoff ensures that we only look at significantly enriched genes in the patient to assign 
them to a Grade of cancer. This query or view result will be used for all further queries into other parts of the database
Save query with a meaningful name PatientTestCancerGrade_query1*/
SELECT CancerGradeUniqTable.gene, 
       CancerGradeUniqTable.Given_Grade, 
       CancerGradeUniqTable.Given_Grade_avgFPKM, 
       CancerGradeUniqTable.otherGrades_avgFPKM, 
       CancerGradeUniqTable.FC_GivenGrade_by_others_Grades,
       Patient_Grade4_test_data.gene, 
       Patient_Grade4_test_data.CGGA_D30, 
       Patient_Grade4_test_data.CGGA_D30/CancerGradeUniqTable.otherGrades_avgFPKM AS FC_patient
FROM 
       CancerGradeUniqTable 
       JOIN Patient_Grade4_test_data ON Patient_Grade4_test_data.gene = CancerGradeUniqTable.gene
WHERE Patient_Grade4_test_data.CGGA_D30/CancerGradeUniqTable.otherGrades_avgFPKM >1.5
ORDER BY CancerGradeUniqTable.Given_Grade DESC
;


/* QUERY2
Here group the PatientTestCancerGrade_query1 by Given_Grade and count. This will give how many of the test patient 
enriched genes belong to Grade 2, 3 or 4. In the present example most of the genes belong to the Grade4, so patient 
is likely Grade4 type.  Note because we are grouping by gene, we are seeign atleast 1 matched row of the query for 
each gene, to see all possible rsults remove the GROUP BY condition.
Save query with a meaningful name PatientPredictedCancerGrade_query2*/
SELECT PatientTestCancerGrade_query1.Given_Grade, 
       Count(PatientTestCancerGrade_query1.gene) AS CountOfgene
FROM PatientTestCancerGrade_query1
GROUP BY PatientTestCancerGrade_query1.Given_Grade
;
/*RESULTS
Given_Grade  CountOfgene
2               113
3               13
4               555
*/


/* QUERY3
In the present example query for the patient enriched cancer genes we can find more functional information on the genes, 
such as Panther Family and full gene name. We can also get some idea about gene expression regulation of these genes by 
querying the known nearest cis-regulatory-regions. The Patient is grade 4, so we filter to keep grade 4 and we also filter 
for FC(fold change) >2. But do not show these in the resulting query table to reduce clutter. Note because we are 
grouping by gene, we are seeign atleast 1 matched row of the query for each gene, to see all possible rsults remove 
the GROUP BY condition.
Save query with a meaningful name PatientTestGeneCisGrade4_query3*/ 
SELECT PatientTestCancerGrade_query1.gene, 
       GeneInformationEdTable.gene,
       GeneInformationEdTable.gene_name_panther,	   
       GeneInformationEdTable.Panther_family, 
       GeneInformationEdTable.Panther_protein_class, 
       CisRegulators5kbTable.CRR_hg19_id, 
       CisRegulators5kbTable.Annotation_of_CRR, 
       CisRegulators5kbTable.Distance_to_TSS, 
       CisRegulators5kbTable.UCSC_genome_browser
FROM PatientTestCancerGrade_query1 
     JOIN GeneInformationEdTable ON GeneInformationEdTable.gene = PatientTestCancerGrade_query1.gene
     JOIN CisRegulators5kbTable ON CisRegulators5kbTable.gene = PatientTestCancerGrade_query1.gene
WHERE PatientTestCancerGrade_query1.Given_Grade=4 
      AND PatientTestCancerGrade_query1.FC_patient >2
GROUP BY PatientTestCancerGrade_query1.gene
; 


/* QUERY4
In the present query for the patient enriched cancer genes we can find potential drugs and diseases associated with the 
diseases. Patient is grade 4, so we filter to keep grade 4 and we also filter for FC(fold change) >2. However, we do not 
show these in the resulting query table to reduce clutter. Note because we are grouping by gene, we are seeign atleast 1 
matched row of the query for each gene, to see all possible rsults remove the GROUP BY condition.
Save query with a meaningful name PatientTestGeneDiseaseDrugGrade4_query4*/
SELECT PatientTestCancerGrade_query1.gene, 
       DiseaseAssociationEdTable.doid_code, 
       DiseaseAssociationEdTable.doid_name, 
       DrugInteractionEdTable.MOLECULE_CHEMBL_ID, 
       DrugInteractionEdTable.drug_name, 
       DrugInteractionEdTable.interaction_types, 
       DrugInteractionEdTable.MOLECULE_TYPE, 
       DrugInteractionEdTable.MAX_PHASE
FROM PatientTestCancerGrade_query1 
    JOIN DiseaseAssociationEdTable ON DiseaseAssociationEdTable.gene = PatientTestCancerGrade_query1.gene
    JOIN DrugInteractionEdTable ON DrugInteractionEdTable.gene = PatientTestCancerGrade_query1.gene
WHERE PatientTestCancerGrade_query1.Given_Grade=4 
      AND PatientTestCancerGrade_query1.FC_patient>2
GROUP BY PatientTestCancerGrade_query1.gene
;

